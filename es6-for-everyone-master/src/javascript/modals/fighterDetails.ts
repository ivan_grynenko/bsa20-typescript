import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { IFighterDetail } from '../../models/fighterDetail';

export  function showFighterDetailsModal(fighter: IFighterDetail): void {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: IFighterDetail): HTMLElement {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameBlockElement = createElement({ tagName: 'div', className: 'name-block' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attributesElement = createElement({ tagName: 'ul' });

  nameElement.innerText = name;
  fighterDetails.append(nameElement);
  fighterDetails.append(nameBlockElement);
  nameBlockElement.appendChild(nameElement);
  fighterDetails.append(attributesElement);

  const healthElement = createElement({ tagName: 'li' });
  healthElement.innerText = `Health: ${health}`;
  attributesElement.appendChild(healthElement);

  const attackElement = createElement({ tagName: 'li' });
  attackElement.innerText = `Attack: ${attack}`;
  attributesElement.appendChild(attackElement);

  const defenseElement = createElement({ tagName: 'li' });
  defenseElement.innerText = `Defence: ${defense}`;
  attributesElement.appendChild(defenseElement);

  const imageElement = createElement({ tagName: 'img', className: "info-pic" });
  imageElement.setAttribute('src', String(source));
  fighterDetails.append(imageElement);

  return fighterDetails;
}