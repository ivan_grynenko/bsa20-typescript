import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { IFighterDetail } from '../../models/fighterDetail';

export function showWinnerModal(fighter: IFighterDetail): void {
  const title = 'Victory!';
  const bodyElement = createWinnerInfo(fighter);
  showModal({ title, bodyElement });
}

function createWinnerInfo(fighter: IFighterDetail): HTMLElement {
  const { name, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameBlockElement = createElement({ tagName: 'div', className: 'name-block' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  nameElement.innerText = name;
  fighterDetails.append(nameBlockElement);
  nameBlockElement.appendChild(nameElement);

  const imageElement = createElement({ tagName: 'img', className: "info-pic" });
  imageElement.setAttribute('src', String(source));
  fighterDetails.append(imageElement);

  return fighterDetails;
}