import { callApi, getFighterById } from '../helpers/apiHelper';
import { IFighterDetail } from '../../models/fighterDetail';

export async function getFighters(): Promise<unknown> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<IFighterDetail | undefined> {
  const endpoint = `details/fighter/${id}.json`;
  return await getFighterById(endpoint);
}