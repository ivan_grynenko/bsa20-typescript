export function fight(firstFighter, secondFighter) {
  let fighter1 = firstFighter;
  let fighter2 = secondFighter;
  let fighters = new Array();
  fighters.push(fighter1);
  fighters.push(fighter2);
  const firstAttacker = Math.floor(Math.random() * 2);
  const secondAttacker = firstAttacker === 0 ? 1 : 0;

  while (true) {
    const firstHit = getDamage(fighters[firstAttacker], fighters[secondAttacker]);
    fighters[secondAttacker].health -= firstHit < 0 ? 0 : firstHit;
    console.log(JSON.stringify(fighters[secondAttacker]));

    if (fighters[secondAttacker].health <= 0) {
      return fighter2;
    }

    const secondHit = getDamage(fighters[secondAttacker], fighters[firstAttacker]);
    fighters[firstAttacker].health -= secondHit < 0 ? 0 : secondHit;
    console.log(JSON.stringify(fighters[firstAttacker]));

    if (fighters[firstAttacker].health <= 0) {
      return fighter1;
    }
  }
}

export function getDamage(attacker, enemy) {
  return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter) {
  const criticalHitChance  = Math.floor(Math.random() * 2 + 1);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.floor(Math.random() * 2 + 1);
  return fighter.defense * dodgeChance;
}
