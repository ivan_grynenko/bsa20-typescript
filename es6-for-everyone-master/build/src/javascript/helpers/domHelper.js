export function createElement(elementInfo) {
    const element = document.createElement(elementInfo.tagName);
    if (elementInfo.className) {
        element.classList.add(elementInfo.className);
    }
    Object.keys(elementInfo.attributes).forEach(key => element.setAttribute(key, elementInfo.attributes[key]));
    return element;
}
