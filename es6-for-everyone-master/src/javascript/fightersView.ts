import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';
import { IFighter } from '../models/fighter';
import { IFighterDetail } from '../models/fighterDetail';

export function createFighters(fighters: IFighter[]): HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event: MouseEvent, fighter: IFighter): Promise<void> {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(<IFighterDetail>fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<IFighterDetail | undefined> {
  return await getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map<String, IFighterDetail>();

  return async function selectFighterForBattle(event: any, fighter: IFighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, <IFighterDetail>fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const arrayOfFighters = [...selectedFighters.values()];
      const winner = fight(arrayOfFighters[0], arrayOfFighters[1]);
      showWinnerModal(winner);
    }
  }
}