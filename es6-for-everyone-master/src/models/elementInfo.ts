export interface IElementInfo {
    tagName: string,
    className?: string,
    attributes?: any
}